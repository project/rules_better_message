<?php

/**
 * @file
 * Rules actions definitions for rules better message module
 */

/**
 * Implementation of hook_rules_action_info().
 */
function rules_better_message_rules_action_info() {
  return array(
    'rules_better_message_send_message' => array(
      'label' => t('Send a message'),
      'module' => 'Messages',
      'eval input' => array(
        'message',
        'type',
        'clean',
        'other',
        'reapeat',
      ),
    ),
    'rules_better_message_clean_messages' => array(
      'label' => t('Clean messages'),
      'module' => 'Messages',
      'eval input' => array(
        'clean',
        'type',
        'other',
      ),
    ),
  );
}

function rules_better_message_send_message($settings) {
  $repeat = $settings['repeat'];
  $type = $settings['type'];
  if (isset($settings['other']) && ($settings['other'] != '')) {
    $type = $settings['other'];
  }
  rules_better_message_clean_messages($settings);
  drupal_set_message(filter_xss($settings['message']), $type, $repeat);
}

function rules_better_message_clean_messages($settings) {
  if ($settings['clean'] == 1) {
    drupal_get_messages();
  } 
  elseif ($settings['clean'] == 2) {
    $type = $settings['type'];
    if (isset($settings['other']) && ($settings['other'] != '')) {
      $type = $settings['other'];
    }
    drupal_get_messages($type);
  }
}
