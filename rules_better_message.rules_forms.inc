<?php

/**
 * @file
 * Rules forms for rules better message module.
 */

function rules_better_message_send_message_form($settings = array(), &$form) {

  $form['settings']['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#default_value' => $settings['message'],
    '#description' => t("The message to the user."),
  );
  $form['settings']['clean'] = array(
    '#type' => 'radios',
    '#title' => t('Clean messages'),
    '#options' => array(
      0 => t('Do not clean'),
      1 => t('Clean all'),
      2 => t('Clean selected type'),
    ),
    '#default_value' => ($settings['clean'] ? $settings['clean'] : 0),
  );
  $form['settings']['type'] = array(
    '#title' => t('Type'), 
    '#type' => 'select',
    '#default_value' => $settings['type'],
    '#options' => array(
      'status' => t('Status'),
      'warning' => t('Warning'),
      'error' => t('Error'),
      'other' => t('Other'),
    ),
    '#multiple' => FALSE,
  );
  $form['settings']['other'] = array(
    '#title' => t('Other type'), 
    '#type' => 'textfield',
    '#default_value' => $settings['other'],
    '#description' => t("Set the type instead of selecting from the list"),
  );
  $form['settings']['repeat'] = array(
    '#title' => t('Repeat'), 
    '#type' => 'checkbox',
    '#default_value' => $settings['repeat'],
    '#description' => t("If this is FALSE and the message is already set, then the message won't be repeated."),
  );
}

function rules_better_message_clean_messages_form($settings = array(), &$form) {

  $form['settings']['clean'] = array(
    '#type' => 'radios',
    '#title' => t('Clean messages'),
    '#options' => array(
      1 => t('Clean all'),
      2 => t('Clean selected type'),
    ),
    '#default_value' => ($settings['clean'] ? $settings['clean'] : 1),
  );
  $form['settings']['type'] = array(
    '#title' => t('Type'), 
    '#type' => 'select',
    '#default_value' => $settings['type'],
    '#options' => array(
      'status' => t('Status'),
      'warning' => t('Warning'),
      'error' => t('Error'),
      'other' => t('Other'),
    ),
    '#multiple' => FALSE,
  );
  $form['settings']['other'] = array(
    '#title' => t('Other type'), 
    '#type' => 'textfield',
    '#default_value' => $settings['other'],
    '#description' => t("Set the type instead of selecting from the list"),
  );
}
